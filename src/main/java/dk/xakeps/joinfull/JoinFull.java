/*
 *     Copyright 2019, Xakep_SDK
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.joinfull;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.IsCancelled;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.ban.BanService;
import org.spongepowered.api.util.Tristate;

import java.util.Optional;

@Plugin(id = "joinfull",
        name = "JoinFull",
        version = "1.0",
        description = "Ability to join full server",
        url = "https://spongeapi.com",
        authors = "Xakep_SDK")
public class JoinFull {
    @Listener
    @IsCancelled(Tristate.UNDEFINED)
    public void onJoin(ClientConnectionEvent.Login event) {
        if (Sponge.getServer().getOnlinePlayers().size() >= Sponge.getServer().getMaxPlayers()) {
            Optional<BanService> provide = Sponge.getServiceManager().provide(BanService.class);
            if (provide.isPresent()) {
                if (provide.get().isBanned(event.getProfile()) || provide.get().isBanned(event.getConnection().getAddress().getAddress())) {
                    return;
                }
            }
            if (event.getTargetUser().hasPermission("joinfull.force")) {
                event.setCancelled(false);
            }
        }
    }
}
